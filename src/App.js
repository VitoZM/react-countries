import CountryForm from "./components/CountryForm/CountryForm";

function App() {
  return (
    <div>
      <CountryForm />
    </div>
  );
}

export default App;
