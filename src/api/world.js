import axios from "axios";

export default axios.create({
  baseURL: "https://backbone-challenge.tk/api",
});