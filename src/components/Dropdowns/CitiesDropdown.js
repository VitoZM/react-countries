import React, { useEffect, useState } from "react";

import world from "../../api/world";

import { Dropdown } from "semantic-ui-react";

const CitiesDropdown = (props) => {
  const [cities, setCities] = useState([]);

  const getCity = (data) => {
    let city_id = data.value;
    return cities.find((city) => city.key === city_id);
  };

  useEffect(() => {
    
    const getCities = async (state) => {
      if (state === null) {
        setCities([]);
        return;
      }
      const response = await world.get("/cities/readByState/" + state, {});

      setCities(
        response.data.cities.map((city) => {
          let { name, id } = city;
          return {
            key: id,
            value: id,
            text: name,
          };
        })
      );
    };
    getCities(props.state);
  }, [props.state]);

  return (
    <Dropdown
      placeholder="Select City"
      fluid
      search
      selection
      options={cities}
      disabled={cities.length === 0 ? true : false}
      onChange={(e, data) => props.onDropdownChange(getCity(data))}
    />
  );
};

export default CitiesDropdown;
