import React, { useEffect, useState } from "react";

import world from "../../api/world";

import { Dropdown } from "semantic-ui-react";

const StatesDropdown = (props) => {
  const [states, setStates] = useState([]);

  const getState = (data) => {
    let state_id = data.value;
    return states.find((state) => state.key === state_id);
  };

  useEffect(() => {
    const getStates = async (country) => {
      if (country === null) {
        setStates([]);
        return;
      }
      const response = await world.get("/states/readByCountry/" + country, {});

      setStates(
        response.data.states.map((state) => {
          let { name, id } = state;
          return {
            key: id,
            value: id,
            text: name,
          };
        })
      );
    };
    getStates(props.country);
  }, [props.country]);

  return (
    <Dropdown
      placeholder="Select State"
      fluid
      search
      selection
      options={states}
      disabled={states.length === 0 ? true : false}
      onChange={(e, data) => props.onDropdownChange(getState(data))}
    />
  );
};

export default StatesDropdown;
